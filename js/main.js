'use strict';

document.addEventListener('DOMContentLoaded', function () {

    var inputs = document.querySelectorAll('.input_block input, .input_block textarea');

    inputs.forEach(function (input) {

        input.addEventListener('input', function () {            
            this.value ? this.classList.add('has_value') : this.classList.remove('has_value');
        });
    });

    $('.calendar').datepicker({ //air datepicker
        language: {                    
            days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
            daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
            daysMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            months: ['January','February','March','April','May','June', 'July','August','September','October','November','December'],
            monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            today: 'Today',
            clear: 'Clear',
            dateFormat: 'mm/dd/yyyy',
            timeFormat: 'hh:ii aa',
            firstDay: 0
        },
        onSelect: function(formattedDate, date) {
            $('.calendar').removeClass('error');
            $('.calendar').val() ?  $('.calendar').addClass('has_value') : $('.calendar').removeClass('has_value')                    
        }
    }) 
    
    
    var face = document.querySelector('#face');

    face.addEventListener('change', function(e) {
        if (this.files[0].name) {
            $('label[for="face"]').removeClass('error')
        }
      $('label[for="face"] .filename')
        .text(this.files[0].name)
        
        $('label[for="face"] .icon_file').hide();
    
    }, false)


    var song = document.querySelector('#song');

    song.addEventListener('change', function(e) {
        if (this.files[0].name) {
            $('label[for="song"]').removeClass('error')
        }
      $('label[for="song"] .filename')
        .text(this.files[0].name)
        
        $('label[for="song"] .icon_file').hide();
    
    }, false)
    

    $('#form').on('submit', function(e) {
        e.preventDefault();
        if (!$('#face').val()) {
            $('label[for="face"]').addClass('error')
            return false
        } else {
            $('label[for="face"]').removeClass('error')
        }
        if (!$('#song').val()) {
            $('label[for="song"]').addClass('error')
            return false
        } else {
            $('label[for="song"]').removeClass('error')
        }        
        var formData = new FormData($(this)[0]);            
        $.ajax({
            type: 'post',
            url: 'https://telaviv-theater.co.il/wp-content/themes/epixx/promo/send.php',
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $("#spinner").show();
                $('button[type="submit"]').attr("disabled", true);
            },
            success: function(data) {
                // console.log(response)                
                $("#spinner").hide();
                $('button[type="submit"]').attr("disabled", false);
                if (data === "ok") {
                    $("#callback_answer")
                      .text("ההודעה נשלחה בהצלחה.")
                      .removeClass('warning')
                      .addClass("success")
                      .show();
                  } else {
                    $("#callback_answer")
                      .text("אירעה שגיאה. נסה שוב מאוחר יותר")
                      .removeClass('success')
                      .addClass("warning")
                      .show();
                }
            }
        })

    })


});