<?php
 error_reporting(E_ALL);
 ini_set("display_errors", 1);
	if ($_POST) {
		$name = $_POST['name'];
		$surname = $_POST['surname'];
		$age = $_POST['age'];
		$birthday = $_POST['birthday'];
		$height = $_POST['height'];
		$parentName = $_POST['parent_name'];
		$parentPhone = $_POST['parent_phone'];
		$number = $_POST['number'];
		$parentEmail = $_POST['parent_email'];
		$street = $_POST['street'];
		$streetNumber = $_POST['street_number'];
		$city = $_POST['city'];
		$exp = $_POST['experience'];

		/* $face = $_FILES['face']['name'];
		
		$song = $_FILES['song']['name']; */

		require_once 'class.phpmailer.php';

		$mail = new PHPMailer(true);
		try {
			$mail->CharSet = 'utf-8';
			$mail->FromName = 'Telaviv-theater.co.il'; 
			$mail->From = "no-reply@telaviv-theater.co.il";  
			$mail->AddAddress('sava0094@gmail.com'); 
			$mail->AddAddress('rocktlv2019@gmail.com'); 			
			$mail->IsHTML(true);
			$mail->Subject = 'אתר התיאטרון Promo Rock';  
			$mail->Body =  'Name: <b>' . $name . '</b><br>
							Surname: <b>' . $surname . '</b><br>
							Birthday: <b>' . $birthday . '</b><br>
							Age: <b>' . $age . '</b><br>
							Height: <b>' . $height . '</b><br>
							Parent\'s Name: <b>' . $parentName . '</b><br>
							Parent\'s Phone: <b>' . $parentPhone . '</b><br>
							Parent\'s Email: <b>' . $parentEmail . '</b><br>
							Number: <b>' . $number . '</b><br>
							Street: <b>' . $street . '</b><br>
							Street Number: <b>' . $streetNumber . '</b><br>
							City: <b>' . $city . '</b><br>
							Experience: <b>' . $exp . '</b><br>'; 

			if(isset($_FILES['face'])) { 
					if($_FILES['face']['error'] == 0){ 
							$mail->AddAttachment($_FILES['face']['tmp_name'], $_FILES['face']['name']); 
					} 
			} 
			if(isset($_FILES['song'])) { 
				if($_FILES['song']['error'] == 0){ 
						$mail->AddAttachment($_FILES['song']['tmp_name'], $_FILES['song']['name']); 
				} 
			}	 
			
			// отправляем наше письмо 
			if(!$mail->send()) {
				echo 0;
			} else {
				echo 'ok';
			}
		} catch (phpmailerException $e) {
			echo $e->errorMessage(); //Pretty error messages from PHPMailer
		} catch (Exception $e) {
			echo $e->getMessage(); //Boring error messages from anything else!
		}
	}